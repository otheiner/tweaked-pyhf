#TODO: What should be the base image? Change this one.
#Dockerfile
#ARG AB_FIXED_RELEASE=21.2.85-centos7
#FROM atlas/analysisbase:$AB_FIXED_RELEASE
#WORKDIR /usr/src/app
#USER root
#COPY . .
#RUN bash install_python_deps.sh && \
#    rm install_python_deps.sh

FROM pyhf/pyhf:latest
RUN echo "Hello world" && \
    mkdir /athena
#COPY --from=0 /usr/src/app /athena
WORKDIR /workdir
COPY install_python_deps.sh /workdir/
RUN bash install_python_deps.sh && \
    rm /workdir/install_python_deps.sh
ENTRYPOINT ["/bin/bash"]

