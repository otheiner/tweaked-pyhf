#!/usr/bin/env bash

set -e

pip install --upgrade pip
pip install sklearn==0.0
pip install uproot==4.2.3
pip install joblib==1.1.0
pip install numpy==1.21.6
pip install iminuit==2.11.2
pip install matplotlib==3.5.2
#pip install torch==1.11.0
pip install jaxlib==0.3.5 
pip install jax==0.3.7
#pip install pandas==0.24.2
